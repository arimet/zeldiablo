package main;


import java.util.Random;

import modele.Aventurier;
import modele.BombeFantome;
import modele.CaseVide;
import modele.ElementInventaire;
import modele.Ennemi;
import modele.Fantome;
import modele.Personnage;
import modele.PotionVie;
import modele.Salle;
import modele.Troll;
import moteurGraphique.Commande;
import moteurGraphique.MoteurGraphique;

/**
 * 
 * @author Antoine
 *
 */
public class Jeu {
	//Attributs
	public static final int NB_SALLES_A_PARCOURIR = 5;
	public static final int TAILLE_SALLE = 32;
	
	public Aventurier pj;
	private boolean persoBouge;

	public static void main(String[] args) {
		try {
			new MoteurGraphique(new Jeu());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Constructeur du Jeu
	 */
	public Jeu() {
		Salle salle = Salle.generer(TAILLE_SALLE, TAILLE_SALLE, false);
		this.persoBouge=false;

		pj = new Aventurier(0, 0);
		pj.setSalle(salle);

		placerAventurier(pj);
	}

	
	/**
	 * Place l'aventurier a plus ou moins
	 * aleatoirement pour qu'il ne soit pas
	 * sur un mur
	 * @param a
	 */
	public static void placerAventurier(Aventurier a) {
		int xp = a.x, yp = a.y;
		int k = 0;
		while(!(a.getSalle().getElement(xp, yp) instanceof CaseVide)) {
			if((k++ % 2) == 0) {
				xp++;
			} else {
				yp++;
			}
		}

		a.x = xp;
		a.y = yp;
	}


	
	/**
	 * Methode permettant des d�placers les ennemis selon leurs intelligence
	 */
	public void deplacerEnnemisAleatoire(){

		for (Ennemi e : pj.getSalle().listeEnnemi) {
			if(e.estVivant()) {


				if(e.getIntelligence()==1){
					this.aleaBougeinteligence2(e);
				}else this.aleaBougeinteligence1(e);

				if(e instanceof Fantome){
					if (this.estDansPerimetre(pj,e, 4)){
						((Fantome)e).modifierVisible(true);
					} else {
						((Fantome)e).modifierVisible(false);
					}
				}

				if(this.persoPeutAttaquer(pj, e)) {
					e.attaquer(pj);;
				}
			}
		}
	}


	
	/**
	 * Methode qui verifie si un perso peut attaquer un ennemi
	 * @param pj Aventurier
	 * @param e Ennemi
	 * @return boolean 
	 */
	private boolean persoPeutAttaquer(Aventurier pj, Ennemi e) {
		return ((pj.getX() == (e.getX() + 1) && (pj.getY() == e.getY()))
				|| (pj.getX() == (e.getX() - 1) && (pj.getY() == e.getY()))
				|| (pj.getX() == (e.getX()) && (pj.getY() == e.getY() + 1))
				|| (pj.getX() == (e.getX()) && (pj.getY() == e.getY() - 1))
				);
	}


	
	/**
	 * Methode permetant de deplacer un PErsonnage dans une Salle
	 * @param c - Commande a executer en fonction des touches
	 */
	public void deplacerPersonnage(Commande c) {
		if (c.bas){
			this.pj.allerBas();
		}else if (c.haut){
			this.pj.allerHaut();
		}else if (c.droite){
			this.pj.allerDroite();
		}else if (c.gauche){
			this.pj.allerGauche();
		}else if(c.attaque){
			Ennemi cible = null;
			for (Ennemi e : pj.getSalle().listeEnnemi){
				if (this.persoPeutAttaquer(pj, e)){
					cible = (Ennemi)e;
					break;
				}
			}

			if(cible != null) {
				this.pj.attaquer(cible);
			}
		}else if(c.soigner){
			int a=-1;
			if (!this.pj.listeInventaire.isEmpty()){
				for (ElementInventaire e : this.pj.listeInventaire){
					if (e instanceof PotionVie){
						this.pj.ajouterVie(Aventurier.MAX_PV/10);
						a=this.pj.listeInventaire.indexOf(e);
						break;
					}
				}

				if(a != -1) {
					this.pj.listeInventaire.remove(a);
				}
			}
		}
		else if(c.attaquefantome){
			int a=-1;
			if (!this.pj.listeInventaire.isEmpty()){
				for (ElementInventaire e : this.pj.listeInventaire){
					if (e instanceof BombeFantome){
						a=this.pj.listeInventaire.indexOf(e);
						break;
					}
				}
			}
			if(a != -1){
				this.pj.listeInventaire.remove(a);
				for (Ennemi e : pj.getSalle().listeEnnemi){
					if (e instanceof Fantome){
						if (this.estDansPerimetre(pj,e,5)){
							e.perdreVie(10);
						}
					}
				}
			}
		}



		if (c.bas || c.haut || c.droite || c.gauche || c.attaque){
			this.setBouge(true);
		}
	}

	
	/**
	 * Methode priv� retournant si un perso est dans le perimetre d'un autre perso
	 * @param pj
	 * @param m
	 * @param perim
	 * @return boolean 
	 */
	private boolean estDansPerimetre(Personnage pj, Personnage m, double perim) {
		return (pj.distance(m) <= perim);
	}

	
	/**
	 * Methode qui set le boolean persoBouge selon nos besoins
	 * @param a
	 */
	public void setBouge(boolean a){
		this.persoBouge=a;
	}

	
	/**
	 * Methode qui retourne le boolean persoBouge
	 * @return
	 */
	public boolean getBouge(){
		return this.persoBouge;
	}

	
	
	/**
	 * M�thode qui fait bouger al�atoire l ennemi d'intelligence 1
	 * @param e
	 */
	public void aleaBougeinteligence1(Ennemi e){
		Random r = new Random();
		int a = r.nextInt(4);
		switch (a){
		case 0:
			if(this.pj.getY() != (e.getY() + 1)) {
				e.allerBas();
			}
			break;
		case 1:
			if(this.pj.getY() != (e.getY() - 1)) {
				e.allerHaut();
			}
			break;
		case 2:
			if(this.pj.getX() != (e.getX() + 1)) {
				e.allerDroite();
			}
			break;	
		case 3:
			if(this.pj.getX() != (e.getX() - 1)) {
				e.allerGauche();
			}
			break;	

		}
	}

	
	/**
	 * Methode qui fait bouger l ennemi en fonction de la position de l aventurier
	 * @param e
	 */
	public void aleaBougeinteligence2(Ennemi e){
		double distance = this.pj.distance(e);


		if(this.pj.getY() != (e.getY() + 1) && distance<this.pj.distance(new Troll(e.getX(),e.y-1))) {
			e.allerBas();
		}


		if(this.pj.getY() != (e.getY() - 1)&& distance<this.pj.distance(new Troll(e.getX(),e.y+1))) {
			e.allerHaut();
		}


		if(this.pj.getX() != (e.getX() + 1)&& distance<this.pj.distance(new Troll(e.getX()-1,e.getY()))) {
			e.allerDroite();
		}


		if(this.pj.getX() != (e.getX() - 1)&& distance<this.pj.distance(new Troll(e.getX()+1,e.y))) {
			e.allerGauche();
		}


	}
}


