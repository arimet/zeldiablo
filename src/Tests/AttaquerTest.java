package Tests;

import static org.junit.Assert.*;
import modele.Aventurier;
import modele.Monstre;
import org.junit.Before;
import org.junit.Test;

public class AttaquerTest {
	Aventurier a;
	Monstre b;
	@Before
	public void initialisation(){
	a = new Aventurier(5, 5);
	 b = new Monstre(6, 5,0);}
		
	
	/**
	 * Methode testant l'attaque d'un aventurier sur un monstre
	 */
	@Test
	public void testAttaquerAventurier() {
		//Methode a tester
		a.attaquer(b);
		//Verification
		assertEquals("La vie du monstre devrait �tre de 1 ",1,b.rendreVie());
	}
	
	/**
	 * Methode testant l'attaque d'un monstre sur un aventurier
	 */
	@Test
	public void testAttaquerMonstre(){
		//Methode a tester
		b.attaquer(a);
		//Verification
		assertEquals("La vie de l'aventurier devrait �tre de 47 ",47,a.rendreVie());
	}
	
	


}
