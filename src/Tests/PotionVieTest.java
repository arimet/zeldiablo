package Tests;

import static org.junit.Assert.*;
import modele.Aventurier;
import modele.PotionVie;
import modele.Salle;

import org.junit.Test;

public class PotionVieTest {

	@Test
	public void test() {
		Aventurier a = new Aventurier(2, 2);
		a.perdreVie(15);
		Salle c = new Salle(3, 3);
		a.setSalle(c);
		PotionVie b = new PotionVie();
		c.modifieElement(2, 1, b);
		a.allerHaut();
		assertTrue(!a.listeInventaire.isEmpty());
		
		
	}

}
