package Tests;

import static org.junit.Assert.*;
import modele.Aventurier;
import modele.Fantome;
import modele.Monstre;
import modele.Troll;


import org.junit.Test;

public class PersonnageTest {

	

	@Test
	public void test() {
		Aventurier a = new Aventurier(5,5);
		Troll b = new Troll(6,6);
		Fantome c = new Fantome(7,7);
		Monstre d = new Monstre(8,8,1);
		assertTrue("L'Aventurier aurait du exister", a !=null);
		assertTrue("Le Troll aurait du exister", b !=null);
		assertTrue("Le Fantome aurait du exister", c !=null);
		assertTrue("Le Monstre aurait du exister", d !=null);
	}

}
