package Tests;

import static org.junit.Assert.*;
import modele.*;

import org.junit.Test;

public class CaseTest {

	@Test
	public void caseDeclencheurPasDeclenchee() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseOuverturePassage(1) , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		String testEtat = ((CaseDeclencheur)(s.getElement(0, 1))).getEtat();
		
		//Verification
		assertEquals("La case ne devrait pas etre declenchee", "non declenche", testEtat);
	}
	
	@Test
	public void caseDeclencheurDeclenchee() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseOuverturePassage(1) , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.allerGauche();
		String testEtat = ((CaseDeclencheur)(s.getElement(0, 1))).getEtat();
		
		//Verification
		assertEquals("La case devrait etre declenchee", "declenche", testEtat);
	}
	
	@Test
	public void passageSecretFerme() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseOuverturePassage(1) , new CaseVide() , new PassageSecret(1) },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		String testEtat = ((PassageSecret)(s.getElement(2, 1))).getEtat();
		
		//Verification
		assertEquals("Le passage devrait etre ferme", "ferme", testEtat);
	}
	
	@Test
	public void passageSecretOuvert() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseOuverturePassage(1) , new CaseVide() , new PassageSecret(1) },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.allerGauche();
		String testEtat = ((PassageSecret)(s.getElement(2, 1))).getEtat();
		
		//Verification
		assertEquals("Le passage devrait etre ouvert", "ouvert", testEtat);
	}
	
	@Test
	public void plusieursPassageSecret() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseOuverturePassage(2) , new CaseVide() }, 
				{new CaseOuverturePassage(1) , new CaseVide() , new PassageSecret(1) },
				{new CaseVide() , new PassageSecret(2) , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.allerGauche();
		String testEtat1 = ((PassageSecret)(s.getElement(2, 1))).getEtat();
		String testEtat2 = ((PassageSecret)(s.getElement(1, 2))).getEtat();
		
		//Verification
		assertEquals("Le passage 1 devrait etre ouvert", "ouvert", testEtat1);
		assertEquals("Le passage 2 devrait etre ferme", "ferme", testEtat2);
	}
	
	@Test
	public void deplacementSurPassageSecretFerme() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseOuverturePassage(1) , new CaseVide() , new PassageSecret(1) },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.allerDroite();
		int posx=a.getX();
		int posy=a.getY();
		
		//Verification
		assertEquals("L'aventurier devrait etre a x=1",1,posx);
		assertEquals("L'aventurier devrait etre a y=1",1,posy);
	}
	
	@Test
	public void deplacementSurPassageSecretOuvert() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseOuverturePassage(1) , new CaseVide() , new PassageSecret(1) },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.allerGauche();
		a.allerDroite();
		a.allerDroite();
		int posx=a.getX();
		int posy=a.getY();
		
		//Verification
		assertEquals("L'aventurier devrait etre a x=2",2,posx);
		assertEquals("L'aventurier devrait etre a y=1",1,posy);
	}
	
	@Test
	public void casePiegeNonDeclenche() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CasePiege() },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		boolean testEtat = ((CasePiege)(s.getElement(2, 1))).estDeclancher();
		
		//Verification
		assertEquals("Le piege ne devrait pas etre declenchee", false, testEtat);
	}
	
	@Test
	public void casePiegeDeclenche() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CasePiege() },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.allerDroite();
		boolean testEtat = ((CasePiege)(s.getElement(2, 1))).estDeclancher();
		
		//Verification
		assertEquals("Le piege devrait etre declenchee", true, testEtat);
	}
	
	@Test
	public void casePiegeEnleveVie() {
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CasePiege() },
				{new CaseVide() , new CaseVide() , new CaseVide() },
		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.allerDroite();
		int testPdv = a.rendreVie();
		
		//Verification
		assertEquals("L'aventurier devrait avoir 48 points de vie", 48, testPdv);
	}
}
