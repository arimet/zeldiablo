package Tests;

import static org.junit.Assert.*;
import modele.Aventurier;
import modele.BombeFantome;

import modele.Salle;

import org.junit.Test;

public class BombeFantomeTest {

	@Test
	public void test() {
		Aventurier a = new Aventurier(2, 2);
		a.perdreVie(15);
		Salle c = new Salle(3, 3);
		a.setSalle(c);
		BombeFantome b = new BombeFantome();
		c.modifieElement(2, 1, b);
		a.allerHaut();
		assertTrue(!a.listeInventaire.isEmpty());
		
		
	}

}