package Tests;
/**
 * Classe de test poour Aventurier
 */
import static org.junit.Assert.*;
import modele.Aventurier;
import modele.CaseVide;
import modele.ElementSalle;
import modele.Monstre;
import modele.Mur;
import modele.Salle;

import org.junit.Test;

public class AventurierTest {

	/**
	 * Test qui permet de verifier le d�placement vers la droite de l'aventurier
	 */
	@Test
	public void testSeDeplacerDroite(){
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },

		};
		s.setElements(e_salle);
		a.setSalle(s);
		//Appel de la m�thode
		a.allerDroite();
		int posx=a.getX();
		int posy=a.getY();
		//Verification
		assertEquals("L'aventurier devrait �tre � x=2",2,posx);
		assertEquals("L'aventurier devrait �tre � y=1",1,posy);

	}
	/**
	 * Test qui permet de verifier le d�placement vers la gauche de l'aventurier
	 */
	@Test
	public void testSeDeplacerGauche(){
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },

		};
		s.setElements(e_salle);
		a.setSalle(s);
		//Appel de la m�thode
		a.allerGauche();
		int posx=a.getX();
		int posy=a.getY();
		//Verification
		assertEquals("L'aventurier devrait �tre � x=0",0,posx);
		assertEquals("L'aventurier devrait �tre � y=1",1,posy);

	}

	/**
	 * Test qui permet de verifier le d�placement vers le bas de l'aventurier
	 */
	@Test
	public void testSeDeplacerBas(){
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },

		};
		s.setElements(e_salle);
		a.setSalle(s);
		//Appel de la m�thode
		a.allerBas();
		int posx=a.getX();
		int posy=a.getY();
		//Verification
		assertEquals("L'aventurier devrait �tre � x=1",1,posx);
		assertEquals("L'aventurier devrait �tre � y=2",2,posy);

	}

	/**
	 * Test qui permet de verifier le d�placement vers le haut de l'aventurier
	 */
	@Test
	public void testSeDeplacerHaut(){
		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },

		};
		s.setElements(e_salle);
		a.setSalle(s);
		//Appel de la m�thode
		a.allerHaut();
		int posx=a.getX();
		int posy=a.getY();
		//Verification
		assertEquals("L'aventurier devrait �tre � x=1",1,posx);
		assertEquals("L'aventurier devrait �tre � y=0",0,posy);

	}


	/**
	 * Test qui permet de verifier la gestion des collisions mur
	 */
	@Test
	public void testSeDeplacerSurMur() {

		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new Mur() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },

		};
		s.setElements(e_salle);
		a.setSalle(s);
		//Appel de la m�thode
		a.allerHaut();
		int posx=a.getX();
		int posy=a.getY();
		//Verification
		assertEquals("L'aventurier devrait �tre � x=1",1,posx);
		assertEquals("L'aventurier devrait �tre � y=1",1,posy);


	}
	

	/**
	 * Test qui permet de verifier la gestion des collisions bordure
	 */
	@Test
	public void testSeDeplacerSurBordure() {

		// Initialisation

		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },

		};
		s.setElements(e_salle);
		a.setSalle(s);
		//Appel de la m�thode
		a.allerHaut();
		a.allerHaut();
		int posx=a.getX();
		int posy=a.getY();
		//Verification
		assertEquals("L'aventurier devrait �tre � x=1",1,posx);
		assertEquals("L'aventurier devrait �tre � y=0",0,posy);
	}
	
	@Test
	public void testAttaquerMonstre() {
		// Initialisation
		Monstre m = new Monstre(1,2,0);
		Aventurier a = new Aventurier(1,1);
		Salle s = new Salle(3,3);
		ElementSalle[][] e_salle = new ElementSalle[][]{ 
				{new CaseVide() , new CaseVide() , new CaseVide() }, 
				{new CaseVide() , new CaseVide() , new CaseVide() },
				{new CaseVide() , new CaseVide() , new CaseVide() },

		};
		s.setElements(e_salle);
		a.setSalle(s);
		
		//Appel de la methode
		a.attaquer(m);
		a.attaquer(m);
		boolean testEstEnVie = m.estVivant();
		
		//Verification
		assertEquals("Le monstre devrait etre mort", false, testEstEnVie);
	}
}
