package moteurGraphique;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AfficheurGraphique extends JPanel {

	/**
	 * l'image a afficher
	 */
	BufferedImage imageEnCours;
	
	public AfficheurGraphique(int x,int y)
	{
		this.setPreferredSize(new Dimension(x,y));
	}
	
	/**
	 * modifie image a afficher
	 */
	public void modifierImage(BufferedImage im)
	{
		imageEnCours=im;
		repaint();
	}
	
	/**
	 * redefinit la methode paint consiste a dessiner l'image en cours
	 * 
	 * @param g
	 *            graphics pour dessiner
	 */
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(this.imageEnCours, 0, 0, getWidth(), getHeight(), 0, 0,
				getWidth(), getHeight(), null);
		
	}

}
