package moteurGraphique;



import javax.swing.ImageIcon;
import javax.swing.JFrame;


/**
 * cree une interface graphique avec son controleur et son afficheur
 * @author vthomas
 *
 */
public class InterfaceGraphique  {

	/**
	 * l'afficheur lie a la JFrame
	 */
	Dessineur dessin;
	
	/**
	 * le controleur lie a la JFrame
	 */
	ControleurGraphique controleur;
	
	/**
	 * la construction de l'interface grpahique
	 * - construit la JFrame
	 * - construit les Attributs
	 * 
	 */
	public InterfaceGraphique()
	{
		//creation JFrame
		JFrame f=new JFrame("KungFu Donjon");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setIconImage(new ImageIcon("images/yingy.png").getImage());
		
		// creation afficheurGraphique
		AfficheurGraphique affiche=new AfficheurGraphique(800,800);
		this.dessin=new Dessineur(800, 800, affiche);
		f.setContentPane(affiche);
		f.setResizable(false);
		
		//ajout du controleur
		ControleurGraphique controlleurGraph=new ControleurGraphique();
		this.controleur=controlleurGraph;
		affiche.addKeyListener(controlleurGraph);	
		
		//recuperation du focus
		f.pack();
		f.setVisible(true);
		f.getContentPane().setFocusable(true);
		f.getContentPane().requestFocus();
	}
	
	/**
	 * retourne l'afficheur de l'interface construite
	 */
	public Dessineur getAfficheur() {
		return dessin;
	}
	
	/**
	 * retourne le controleur de l'affichage construit
	 * @return
	 */
	public ControleurGraphique getControleur() {
		return controleur;
	}
	
}
