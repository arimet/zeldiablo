package moteurGraphique;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.Jeu;
import modele.Aventurier;
import modele.ElementInventaire;
import modele.Ennemi;
import modele.PotionVie;


/**
 * un afficheur graphique
 * 
 * cela fonctionne avec un principe de doubleBuffering - une image vide est cree
 * en plus du JPanel - on demande d'ajouter des elements dans cette image vide -
 * on demande ensuite d'afficher cette image dans le JPanel
 * 
 * @author vthomas
 */
public class Dessineur  {

	/**
	 * constante pour generer la taille des cases
	 */
	protected int TAILLE_CASE = 25;
	
	
	/**
	 * lien avers l'afficheur dans lequel dessiner
	 */
	AfficheurGraphique affiche; 

	/**
	 * une image intermediaire
	 */
	private BufferedImage imageSuivante;
	private BufferedImage imageEnCours;
	
	private BufferedImage imageHero;
	private BufferedImage mur;
	private BufferedImage monstre;
	private BufferedImage troll;

	

	public BufferedImage piege;
	private BufferedImage ouvert;
	private BufferedImage ferme;
	private BufferedImage sol;
	private BufferedImage fantome;
	private BufferedImage potion;
	private BufferedImage potion2;
	private BufferedImage bombe;
	private BufferedImage bombe2;
	private BufferedImage trophe;
	private BufferedImage sortie;
	
	private Aventurier aventurier;
	/**
	 * la taille des images
	 */
	int width, height;
	
	private final String pdv = "Points de vie";

	private boolean finJeu = false;


	private boolean Mort = false;

	/**
	 * constructeur vide cela construit une image intermediaire et permet de
	 * dessiner dessus
	 */
	public Dessineur(int width, int height, AfficheurGraphique aff) {
		this.width = width;
		this.height = height;
		this.affiche= aff;

		// cree l'image buffer et son graphics
		this.imageSuivante = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		this.imageEnCours = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		
		try {
			chargerImages();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	/**
	 * Methode chargeant les images
	 * @throws IOException
	 */
	public void chargerImages() throws IOException {
		this.imageHero = ImageIO.read(new File("images/hero2.jpg"));
		this.sol = ImageIO.read(new File("images/parquet.jpg"));
		this.mur=ImageIO.read(new File("images/mur.jpg"));
		this.piege=ImageIO.read(new File("images/piege2.jpg"));
		this.monstre=ImageIO.read(new File("images/monstre.jpg"));
		this.ouvert=ImageIO.read(new File("images/bouton_ouvrir.jpg"));
		this.ferme=ImageIO.read(new File("images/bouton_fermer.jpg"));
		this.fantome=ImageIO.read(new File("images/fantome2.jpg"));
		this.troll=ImageIO.read(new File("images/troll2.jpg"));
		this.potion=ImageIO.read(new File("images/potion2.gif"));
		this.potion2=ImageIO.read(new File("images/potion2.jpg"));
		this.bombe=ImageIO.read(new File("images/bombe1.jpg"));
		this.bombe2=ImageIO.read(new File("images/bombe.png"));
		this.trophe=ImageIO.read(new File("images/trophe.jpg"));
		this.sortie=ImageIO.read(new File("images/sortie2.jpg"));
	}

	/**
	 * dessiner un objet consiste a utiliser un sprite
	 */
	public void dessinerObjet(String s, int x, int y) {
		Graphics2D crayon = (Graphics2D) imageSuivante.getGraphics();
		switch (s) {
		case "PJ":
			crayon.drawImage(this.imageHero, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "MJ":
			crayon.drawImage(this.monstre, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "FANTOME": 
			crayon.drawImage(this.fantome, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "MUR":
			crayon.drawImage(this.mur, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "SOL":
			crayon.drawImage(this.sol, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "OUVERTURE":
			crayon.drawImage(this.ouvert, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "FERMETURE":
			crayon.drawImage(this.ferme, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "PIEGE":
			crayon.drawImage(this.piege, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "PASSAGESECRET" : 
			crayon.drawImage(this.sol, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "TROLL" : 
			crayon.drawImage(this.troll, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "AMULETTE":
			crayon.drawImage(this.trophe, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "POTIONVIE":
			crayon.drawImage(this.potion2, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "BOMBEFANTOME":
			crayon.drawImage(this.bombe, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
			break;
		case "SORTIE":
			crayon.drawImage(this.sortie, x*TAILLE_CASE, y*TAILLE_CASE, affiche);
		}
	}

	/**
	 * On inverse les images celle a dessiner et celle temporaire
	 */
	public void rendreImage() {
		BufferedImage temp = this.imageEnCours;
     	// l'image a dessiner est celle qu'on a construite
		this.imageEnCours = this.imageSuivante;
		// l'ancienne image est videe
		this.imageSuivante = temp;
		this.imageSuivante.getGraphics().fillRect(0, 0, this.width,this.height);

		this.dessinerJaugeVie(this.imageEnCours.getGraphics());
		this.dessinerInventaire(this.imageEnCours.getGraphics());
		
		if(this.finJeu) {
			this.dessinerFinJeu(this.imageEnCours.getGraphics());
		}
		
		if(this.Mort ){
			this.dessinerMort(this.imageEnCours.getGraphics());
		}
	
		affiche.modifierImage(imageEnCours);
	}
	
	
	/**
	 * Methode qui permet de dessiner l ecran de Fin
	 * @param g
	 */
	private void dessinerFinJeu(Graphics g) {
		String msg = "YOU WIN !";
		
		g.setFont(new Font("Rockwell", Font.BOLD, 50));
		g.setColor(Color.white);
		g.drawString(msg, this.width / 2 - msg.length() * 15, this.height / 2);
	}
	
	
	/**
	 * Methode qui permet de dessiner l ecran de Mort
	 * @param g
	 */
	private void dessinerMort(Graphics g) {
		String msg = "YOU LOSE !";
		
		g.setFont(new Font("Rockwell", Font.BOLD, 50));
		g.setColor(Color.white);
		g.drawString(msg, this.width / 2 - msg.length() * 15, this.height / 2);
	}
	
	
/**
 * Methode qui permet de desiiner l inventaire
 * @param g
 */
	private void dessinerInventaire(Graphics g) {
		g.setColor(Color.black);
		// Le fond
		g.drawRect(this.width - pdv.length() * 5 - 10, 30, pdv.length() * 5 + 10, 30);
		g.drawRect(this.width - pdv.length() * 5 - 10, 30, ((pdv.length() * 5 + 10) / 2) + 1, 30);
		g.setColor(new Color(0, 0, 0, 90));
		g.fillRect(this.width - pdv.length() * 5 - 10, 30, pdv.length() * 5 + 10, 30);
		
		int xPotions = (this.width - pdv.length() * 5 - 10 + 5), xBombes = this.width - (((pdv.length() * 5 + 10) / 2) + 1), y = 33;
		int nbPotions = 0, nbBombes = 0;
		for(ElementInventaire ei : this.aventurier.listeInventaire) {
			if(ei instanceof PotionVie) {
				nbPotions++;
				g.drawImage(this.potion, xPotions, y, null);
			} else {
				nbBombes++;
				g.drawImage(this.bombe2, xBombes+5, y, null);
			}
		}
		
		g.setColor(Color.white);
		g.setFont(new Font("Rockwell", Font.BOLD, 9));
		
		if(nbPotions > 0) {
			g.drawString("x" + String.valueOf(nbPotions), xPotions+18, y+10);
		}
		
		if(nbBombes > 0) {
			g.drawString("x" + String.valueOf(nbBombes), xBombes+28, y+10);
		}
	}
	
	
	/**
	 * Methode qui permet de dessiner la Jauge De Vie
	 * @param g
	 */
	private void dessinerJaugeVie(Graphics g) {
		g.setColor(Color.black);
		// Le fond
		g.drawRect(this.width - pdv.length() * 5 - 10, 0, pdv.length() * 5 + 10, 30);
		g.setColor(new Color(0, 0, 0, 90));
		g.fillRect(this.width - pdv.length() * 5 - 10, 0, pdv.length() * 5 + 10, 30);
		
		// Les points de vie
		g.setColor(Color.red);
		g.fillRect(this.width - pdv.length() * 5 - 5, 15, (this.aventurier.rendreVie() * (pdv.length() * 5)) / Aventurier.MAX_PV, 10);
		
		g.setColor(Color.black);
		// La jauge de vie
		g.drawRect(this.width - pdv.length() * 5 - 5, 15, pdv.length() * 5, 10);
		
		// Le texte
		g.setFont(new Font("Rockwell", Font.BOLD, 10));
		g.setColor(Color.white);
		g.drawString(pdv, this.width - pdv.length() * 5 - 5, 10);
	}

	
	/**
	 * methode dessiner a completer
	 * 
	 * @param j
	 *            jeu a dessiner
	 */
	public void dessiner(Jeu j) {
		for(int y = 0; y < j.pj.getSalle().getHauteur(); y++) {
			for(int x = 0; x < j.pj.getSalle().getLargeur(); x++) {
				String infoElement = j.pj.getSalle().getElement(x,y).descripitifElement();
				switch(infoElement){
				case "Mur":
					this.dessinerObjet("MUR", x, y);
				break;
				case "CaseVide":
					this.dessinerObjet("SOL", x, y);
				break;
				case "CasePiege false":
					this.dessinerObjet("SOL", x, y);
				break;
				case "CaseOuverturePassage":
					this.dessinerObjet("OUVERTURE", x, y);
				break;
				case "CaseFermeturePassage":
					this.dessinerObjet("FERMETURE", x, y);
				break;
				case "CasePiege true":
					this.dessinerObjet("PIEGE", x, y);
				break;
				case "PassageSecret":
					this.dessinerObjet("PASSAGESECRET", x, y);
				break;
				case "Amulette":
					this.dessinerObjet("AMULETTE", x, y);
					break;
				case "PotionVie":
					this.dessinerObjet("POTIONVIE", x, y);
					break;
				case "BombeFantome":
					this.dessinerObjet("BOMBEFANTOME", x, y);
					break;
				case "CasePorte":
					this.dessinerObjet("SORTIE", x, y);
				}
				
				if(((j.pj.x == x) && (j.pj.y == y)) && (j.pj.rendreVie() > 0)) {
					this.dessinerObjet("PJ", x, y);
				}
				
				for(Ennemi e : j.pj.getSalle().listeEnnemi) {
					if((e.getX() == x) && (e.getY() == y)) {
						if((e.estVivant())) {
							if(e.descripitifElement().equals("Monstre")){
							this.dessinerObjet("MJ", x, y);
							}
							if(e.descripitifElement().equals("Fantome true")){
								this.dessinerObjet("FANTOME", x, y);
							}
							if(e.descripitifElement().equals("Troll")){
								this.dessinerObjet("TROLL", x, y);
							}
						}
					} 
				}
			}
		}
		
		this.rendreImage();
		
	}
	
	/**
	 * Methode qui set un aventurier
	 * @param a
	 */
	public void setAventurier(Aventurier a) {
		this.aventurier = a;
	}

	
	/**
	 * Methode qui met finJeu a true
	 */
	public void finJeu() {
		this.finJeu = true;
	}
	
	/**
	 * Methode qui met Mort a true
	 */
	public void Mort() {
		this.Mort = true;
	}

}
