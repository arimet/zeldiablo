package moteurGraphique;

import main.Jeu;
import modele.Aventurier;
import modele.Salle;

public class MoteurGraphique {
	/**
	 * Attributs
	 */
	Jeu j;
	
	
	/**
	 * Constructeur de MoteurGraphique
	 * @param j
	 * @throws InterruptedException
	 */
	public MoteurGraphique(Jeu j) throws InterruptedException{
		//creation du jeu
		this.j=j;
		
		//creation du moteur graphique
		InterfaceGraphique inter=new InterfaceGraphique();
		ControleurGraphique controle=inter.getControleur();
		
		Dessineur affiche=inter.getAfficheur();
		affiche.setAventurier(j.pj);
		
		int i = 0;
		//boucle de jeu
		while(true)
		{	
			Commande c=controle.getCommande();
			j.deplacerPersonnage(c);
			
			if(i == 3) {
				i = 0;
				if(j.getBouge()){
					j.deplacerEnnemisAleatoire();
					j.setBouge(false);
				}
			} else {
				i++;
			}
			
			if (!j.pj.estVivant()) {
				affiche.Mort();
				affiche.dessiner(j);
				break;
			}
			
			if(j.pj.getSalle().getElement(j.pj.getX(), j.pj.getY()).descripitifElement().equals("CasePorte")) {
				j.pj.setSalle(Salle.generer(Jeu.TAILLE_SALLE, Jeu.TAILLE_SALLE, (Aventurier.NOMBRE_SALLES_PARCOURUES == Jeu.NB_SALLES_A_PARCOURIR)));
				
				j.pj.x = 0;
				j.pj.y = 0;
				
				Jeu.placerAventurier(j.pj);
			} else if (j.pj.getSalle().getElement(j.pj.getX(), j.pj.getY()).descripitifElement().equals("Amulette")) {
				affiche.finJeu();
				affiche.dessiner(j);
				break;
			}
			
			affiche.dessiner(j);
			Thread.sleep(100);
		}
	}

}
