/**
 * 
 */
package modele;

/**
 * Classe Mur qui repr�sente les cases vides du labyrinthe
 * @author kartner1u
 *
 */
public class CaseVide implements ElementSalle{
	
	/**
	 * methode qui affiche la case vide
	 */
	public String descripitifElement(){
		return "CaseVide";
	}

}
