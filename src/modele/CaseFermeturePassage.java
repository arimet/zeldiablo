package modele;
/**
 *Classe definissant CaseFermeture herite de CaseDeclencheur
 */
public class CaseFermeturePassage extends CaseDeclencheur {

	/**
	 * Constructeur de CaseFermuturePassage
	 * @param liaison
	 */
	public CaseFermeturePassage(int liaison) {
		super(liaison);
	}

	/**
	 * Methode de CaseDeclencheur definie pour CaseFermeturePassege
	 */
	public void declenchement() {
		this.etat = "declenche";
	}
	
	/**
	 * Methode donnant le nom de la classe
	 */
	public String descripitifElement(){
		return "CaseFermeturePassage";
	}

}