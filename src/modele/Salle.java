package modele;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import main.Jeu;

/**
 * Represente une salle du labyrinthe
 * Elle est definie par sa hauteur, sa largeur et ses elements (du type ElementSalle) 
 * @author strauss3u
 *
 */
public class Salle {
	
	public ArrayList<Ennemi> listeEnnemi;
	/**
	 * La hauteur de la salle
	 */
	private int hauteur;
	
	/**
	 * La largeur de la salle
	 */
	private int largeur;
	
	/**
	 * Les elements qui composent
	 * la salle
	 */
	private ElementSalle[][] elements;
	
	private static Random rand = new Random();
	private static List<Point> passages = new ArrayList<>();
	
	/**
	 * Constructeur
	 * @param h La hauteur de la salle
	 * @param l La largeur de la salle
	 */
	public Salle(int h, int l) {
		this.hauteur = h;
		this.largeur = l;
		this.listeEnnemi=new ArrayList<Ennemi>();
		this.elements = new ElementSalle[this.hauteur][this.largeur];
		for(int y = 0; y < this.hauteur; y++) {
			for(int x = 0; x <this.largeur; x++) {
				this.elements[y][x] = new CaseVide();
			}
			
		}
	}
	
	/**
	 * Permet de recuperer un element
	 * de la salle
	 * @param x La coordonnee x de l'element (la colonne)
	 * @param y La coordonnee y de l'element (la ligne)
	 * @return L'elemeent a la place (x, y) dans la salle, ou null si x et y ne sont pas dans la salle
	 */
	public ElementSalle getElement(int x, int y) {
		ElementSalle el;
		
		try {
			el = this.elements[y][x];
		} catch(IndexOutOfBoundsException e) {
			el = null;
		}
		
		return el;
	}
	
	
	/**
	 * Methode retournant elements
	 * @return
	 */
	public ElementSalle[][] getElements() {
		return this.elements;
	}
	
	/**
	 * Affiche la salle dans laquelle l'aventurier
	 * passe en parametre est actuellement
	 * @param a L'aventurier qu'on veut afficher dans sa salle actuelle
	 */
	public static void afficher(Aventurier a) {
		Salle s = a.getSalle();
		
		if(s != null) {
			System.out.print(" ");
			for(int i = 0; i < s.getLargeur(); i++) {
				System.out.print("-");
			}
			
			System.out.println();
			for(int y = 0; y < s.getHauteur(); y++) {
				System.out.print("|");
				for(int x = 0; x < s.getLargeur(); x++) {
					if((x == a.getX()) && (y == a.getY())) {
						System.out.print(a);
					} else {
						System.out.print(s.getElement(x, y));
					}
				}
				System.out.println("|");
			}
			
			System.out.print(" ");
			for(int i = 0; i < s.getLargeur(); i++) {
				System.out.print("-");
			}
		}
	}
	
	/**
	 * Permet de specifier les elements qui compose la salle
	 * La methode verifie que le tableau passe en parametre
	 * a les bonnes dimensions
	 * @param els Les elements
	 */
	public void setElements(ElementSalle[][] els) {
		if((els.length == this.hauteur) && ((els.length >= 1) && (els[0].length == this.largeur))) {
			this.elements = els;
		}
	}
	
	/**
	 * @return La hauteur de la salle
	 */
	public int getHauteur() {
		return this.hauteur;
	}
	
	/**
	 * @return La largeur de la salle
	 */
	public int getLargeur() {
		return this.largeur;
	}
	
	
	/**
	 * Permet de modifier un element a des coordonees precise par un autre element
	 * @param x
	 * @param y
	 * @param es
	 */
	public void modifieElement(int x, int y, ElementSalle es){
		this.elements[y][x]=es;
	}
	
	
	/**
	 * Permet de generer un salle
	 * @param l
	 * @param h
	 * @param amulette
	 * @return
	 */
	public static Salle generer(int l, int h, boolean amulette) {
		Salle s = new Salle(h, l);
		
		ElementSalle[][] els = new ElementSalle[h][l];
		for(int i=0;i<els.length;i++){
			for(int j=0;j<els[0].length;j++){
				switch (rand(30)){
				case 5 :
					els[i][j]= new CasePiege();
					break;
				default:
					els[i][j]= new CaseVide();
				}
			}
		}
		
		s.setElements(els);
		
		diviser(s.getElements(), 0, 0, l, h, choixOrientation(l, h));
		
		els = s.getElements();
		
		int rx = rand(els.length), ry = rand(els[0].length);
		for(int k = 0; k < 2; k++) {
			// Ajout d'un passage entre les murs
			while(!s.getElement(rx, ry).descripitifElement().equals("Mur")) {
				ry = rand(els.length);
				rx = rand(els[0].length);
			}
			els[ry][rx] = new PassageSecret(k); 
			els[trouverCaseVide(s).y][trouverCaseVide(s).x] = new CaseOuverturePassage(k);
			els[trouverCaseVide(s).y][trouverCaseVide(s).x] = new CaseFermeturePassage(k);
		}
		
		// Ajout des monstres
		for(int k = 0; k < (Jeu.TAILLE_SALLE / 4) + 1; k++) {
			rx = trouverCaseVide(s).x;
			ry = trouverCaseVide(s).y;
			
			Random r = new Random();
			int alea = r.nextInt(3);
			
			int intelAlea=r.nextInt(3);
			switch(alea){
			case 0:
				s.ajouterEnnemi(new Monstre(rx,ry,intelAlea));
				break;
			case 1:
				s.ajouterEnnemi(new Fantome(rx,ry));
				break;
			case 2:
				s.ajouterEnnemi(new Troll(rx,ry));
				break;
			}
			//s.ajouterEnnemi(((k % 4 ) == 0) ? new Monstre(rx, ry) : new Troll(rx, ry) );
			
		}
		
		ElementSalle sortie;
		if(amulette) {
			sortie = new Amulette();
		} else {
			sortie = new CasePorte();
		}
		els[els.length - 1][els[0].length - 1] = sortie;
	
		els[trouverCaseVide(s).y][trouverCaseVide(s).x]= new PotionVie();
		els[trouverCaseVide(s).y][trouverCaseVide(s).x]= new BombeFantome();
		
		passages.clear();
		
		return s;
	}
	
	/**
	 * Trouve un couple (rx, ry) qui pointe sur une case vide
	 * @param s
	 * @return
	 */
	private static Point trouverCaseVide(Salle s) {
		ElementSalle[][] els = s.getElements();
		
		int ry = rand(els.length);
		int rx = rand(els[0].length);
		while(!(s.getElement(rx, ry) instanceof CaseVide)) {
			rx = rand(els.length);
			ry = rand(els[0].length);
		}
		
		return new Point(rx, ry);
	}
	
	/**
	 * Algorithme adapte de : 
	 * http://weblog.jamisbuck.org/2011/1/12/maze-generation-recursive-division-algorithm
	 * @param els
	 * @param x
	 * @param y
	 * @param largeur La largeur
	 * @param hauteur La hauteur
	 * @param o L'orientation
	 */
	private static void diviser(ElementSalle[][] els, int x, int y, int largeur, int hauteur, int o) {
		if((largeur > 2) && (hauteur > 2)) {
			boolean horizontal = (o == 0);
			
			// Les coordonn�es de d�part du mur
			int mx = x + (horizontal ? 0 : rand(largeur-2));
			int my = y + (horizontal ? rand(hauteur-2) : 0);
			
			// Les coordonn�es du passage � travers
			int px = mx + (horizontal ? rand(largeur) : 0);
			int py = my + (horizontal ? 0 : rand(hauteur));
			
			passages.add(new Point(px, py));
			
			for(int j = 0; j < (horizontal ? largeur : hauteur); j++) {
				if((((mx != px) || (my != py))) && !passages.contains(new Point(mx, my)) && !passages.contains(new Point(mx - 1, my))
						&& !passages.contains(new Point(mx, my - 1)) && !passages.contains(new Point(mx + 1, my))
						&& !passages.contains(new Point(mx, my + 1)) && !passages.contains(new Point(mx + 1, my - 1))
						&& !passages.contains(new Point(mx - 1, my + 1)) && !passages.contains(new Point(mx - 1, my - 1))
						&& !passages.contains(new Point(mx + 1 , my +1))) {
					els[my][mx] = new Mur();
				}
				mx += horizontal ? 1 : 0;
				my += horizontal ? 0 : 1;
			}
			
			diviser(els, x, y, horizontal ? largeur : (mx-x+1), horizontal ? (my-y+1) : hauteur, choixOrientation(largeur, hauteur));
			diviser(els, horizontal ? x : (mx+1), horizontal ? (my+1) : y, horizontal ? largeur : (x+largeur-mx-1), horizontal ? (y+hauteur-my-1) : hauteur, choixOrientation(largeur, hauteur));
		}
	}
	
	// 0 = horizontal, 1 : vertical
	private static int choixOrientation(int l, int h) {
		return ((l < h) ? 0 : 1);
	}
	
	/**
	 * Methode retournant un int aleatoire
	 * @param max
	 * @return
	 */
	private static int rand(int max) {
		return rand.nextInt(max);
	}
	/**
	 * Methode permettant d ajouter un ennemi dans la salle
	 * @param e
	 */
	public void ajouterEnnemi(Ennemi e){
		e.setSalle(this);
		this.listeEnnemi.add(e);
	}
}
