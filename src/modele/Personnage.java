package modele;

/**
 * Classe abstract de personnage
 * @author rimet1u
 *
 */

public abstract class Personnage implements ElementSalle {
	/**
	 * Attribut int x qui definit la position en x du personnage
	 */
	public int x;
	/**
	 * Attribut int y qui definit la position en y du personnage
	 */
	public int y;
	
	/**
	 * Attrribut Salle qui definit la salle ou se trouve le personnage
	 */
	private Salle salle_actuelle;
	
	protected int pDv;
	
	/**
	 * Constructeur de Personnage
	 */
	public Personnage(int x,int y){
		this.x=x;
		this.y=y;
		
		this.pDv = 10;
	}
	
	/**
	 *Methode qui definit la salle du Personnage
	 */
	public void setSalle(Salle salle_nouvelle) {
		this.salle_actuelle = salle_nouvelle;
	}
	
	/**
	 * Methode qui retourne la salle ou se trouve le Personnage
	 * @return salle_actuelle
	 */
	public Salle getSalle() {
		return salle_actuelle;
	}
	
	
	/**
	 * Methode permettant au personnage de bouger en fonction de certaines conditions
	 * @param dx La variation en x
	 * @param dy La variation en y
	 */
	protected void seTranslater(int dx, int dy){
		boolean seDeplacer = true;
		for(Ennemi e : this.salle_actuelle.listeEnnemi) {
			if((e.getX() == (this.x + dx)) && (e.getY() == (this.y + dy)) && e.estVivant()) {
				seDeplacer = false;
				break;
			}
		}
		
		ElementSalle dest = this.salle_actuelle.getElement(this.x + dx, this.y + dy);
		if((dest != null) && (!(dest instanceof Mur) || ((dest instanceof Mur) && (this instanceof Fantome)))  && seDeplacer && !(dest instanceof Personnage) && !(((dest instanceof PassageSecret) && (((PassageSecret)(dest)).getEtat() == "ferme")))){	
			this.x += dx;
			this.y += dy;
			
			if(dest instanceof CaseDeclencheur){
				((CaseDeclencheur)(dest)).declenchement();

				for(int i=0;i<this.getSalle().getHauteur();i++){
					for(int j=0;j<this.getSalle().getLargeur();j++){
						if(((this.salle_actuelle.getElement(i, j) instanceof PassageSecret) && ((CaseDeclencheur)(this.salle_actuelle.getElement(this.x, this.y))).getLiaison() == ((PassageSecret)(this.salle_actuelle.getElement(i, j))).getLiaison())){
							if((this.salle_actuelle.getElement(this.x, this.y)) instanceof CaseOuverturePassage){
								((PassageSecret)(this.salle_actuelle.getElement(i, j))).ouverture();
							} else if((this.salle_actuelle.getElement(this.x, this.y)) instanceof CaseFermeturePassage){
								((PassageSecret)(this.salle_actuelle.getElement(i, j))).fermeture();
							}
						}
					}
				}
			}
				
			if((this.salle_actuelle.getElement(this.x, this.y) instanceof CasePiege) && !(this instanceof Fantome)){
				((CasePiege)(this.salle_actuelle.getElement(this.x, this.y))).declenchement();
				if(this instanceof Aventurier || this instanceof Monstre){
					this.perdreVie(2);
				}
			}
			if (this.salle_actuelle.getElement(this.x, this.y) instanceof ElementInventaire && this instanceof Aventurier){
				((Aventurier)this).ajoutElemInventaire((ElementInventaire) (this.salle_actuelle.getElement(this.x, this.y)));
				this.getSalle().modifieElement(this.x, this.y, new CaseVide());

			}
		}
	}
	
	/**
	 * Methode qui permet au Personnage d'aller a droite
	 */
	public void allerDroite(){
		this.seTranslater(1, 0);
	}
	
	/**
	 * Methode qui permet a Personnage d'aller a gauche
	 */
	public void allerGauche(){
		this.seTranslater(-1, 0);
	}
	
	/**
	 * Methode qui permet au Personnage d'aller en haut
	 */
	public void allerHaut(){
		this.seTranslater(0, -1);
	}
	
	/**
	 * Methode qui permet au Personnage d'aller en bas
	 
	 */
	public void allerBas(){
		this.seTranslater(0, 1);
	}
	
	/**
	 * Methode qui renvoi int x
	 * @return int x
	 */
	public int getX(){
		return this.x;
	}
	
	/**
	 * Methode qui renvoi int y
	 * @return int y
	 */
	public int getY(){
		return this.y;
	}
	
	public String toString(){
		return "x";
	}
	
	/**
	 * Methode qui permet au Personnage d attaquer un autre personnage
	 * @param p
	 */
	public void attaquer(Personnage p) {
		if(this.estVivant()) {
			if (this instanceof Fantome){
				p.perdreVie(Aventurier.MAX_PV / 20);
			}if (this instanceof Monstre){
				p.perdreVie(Aventurier.MAX_PV / 15);
			}if (this instanceof Aventurier){
				p.perdreVie(4);
			}if (this instanceof Troll){
				p.perdreVie(Aventurier.MAX_PV / 10);
			}
		}
	}
	
	
	/**
	 * Methode qui fait perdre de la vie au personnage
	 * @param a
	 */
	public void perdreVie(int a){
		this.pDv -= a;
		
		if(this.pDv < 0) {
			this.pDv = 0;
		}
	}
	
	
	/**
	 * Methode qui retourne la vie du personnage
	 * @return
	 */
	public int rendreVie(){
		return this.pDv;
	}

	/**
	 * Methode qui retourne si le personnage est vivant
	 * @return
	 */
	public boolean estVivant() {
		return (this.pDv > 0);
	}
	
	
	/**
	 * Methode qui retourne la distance entre deux personnages
	 * @param p2
	 * @return
	 */
	public double distance(Personnage p2) {
		return Math.sqrt(Math.pow(p2.getX() - this.getX(), 2) + Math.pow(p2.getY() - this.getY(), 2));
	}
}
	



