package modele;

/**
 * Methode qui definit les ennemis herite de la classe Personnage 
 */
public abstract class Ennemi extends Personnage {

	/**
	 * Constructeur Ennemi
	 * @param x
	 * @param y
	 */
	public Ennemi(int x, int y) {
		super(x, y);

	}

	/**
	 * Methode retournant l'inteligence des Ennemi
	 * @return
	 */
	public abstract int getIntelligence();

}



