package modele;
/**
 * Classe de CaseOuverturePassage herite de CaseDeclencheur
 */
public class CaseOuverturePassage extends CaseDeclencheur {

	
	/**
	 * Constructeur de CaseOuverturePassage
	 * @param liaison
	 */
	public CaseOuverturePassage(int liaison) {
		super(liaison);
	}

	/**
	 * Methode venant de casedeclencheur redefinie pour cette classe
	 */
	@Override
	public void declenchement(){
		this.etat = "declenche";
	}
	
	
	/**
	 * Methode retournant le nom de la classe
	 */
	public String descripitifElement(){
		return "CaseOuverturePassage";
	}

}