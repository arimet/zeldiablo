package modele;
/**
 * Methode definisant CaseDeclencheur
 */
public abstract class CaseDeclencheur implements ElementSalle {
	/**
	 * Attributs
	 */
	private int liaison;
	protected String etat;
	
	/**
	 * Constructeur de CaseDeclencheur
	 * @param liaison
	 */
	public CaseDeclencheur(int liaison){
		this.liaison = liaison;
		this.etat = "non declenche";
	}
	

	/**
	 * Methode donnant le nom de la Classe
	 * @return String
	 */
	public String descripitifElement(){
		return "CaseDeclencheur";
	}
	
	
	/**
	 * Methode abstract de declenchement
	 */
	public abstract void declenchement();
	
	
	/**
	 * Getter retournant la liason de CaseDeclencher
	 * @return
	 */
	public int getLiaison(){
		return liaison;
	}
	
	
	/**
	 * Methode retournant l etat de CaseDeclencheur
	 * @return
	 */
	public String getEtat(){
		return etat;
	}
}
