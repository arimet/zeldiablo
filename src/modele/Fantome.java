package modele;
/**
 * Classe definisant les fantomes herite d ennemi
 *
 */
public class Fantome extends Ennemi {
	/**
	 * Attributs
	 */
	private boolean visible;


	/**
	 * Constructeur de Fantome
	 * @param x
	 * @param y
	 */
	public Fantome(int x, int y) {
		super(x, y);
		this.visible=false;

	}

	
	/**
	 * Methode qui retourne le descriptif de Fantome
	 */
	public String descripitifElement() {
		String s = "Fantome " + this.visible;
		return s;
	}

	
	/**
	 * Methode qui retourne si le fantome est visible
	 * @return
	 */
	public boolean estVisible(){
		return this.visible;
	}

	
	/**
	 * Methode qui modifi la visibilite du fantome
	 * @param b
	 */
	public void modifierVisible(boolean b){
		this.visible = b;
	}

	@Override
	public int getIntelligence() {

		return 0;
	}


}
