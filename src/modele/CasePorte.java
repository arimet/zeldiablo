package modele;

/**
 *Classe definissant CasePorte qui possede l interface ElementSalle
 */
public class CasePorte implements ElementSalle {

	/**
	 * Methode retournant le descriptif de la salle
	 */
	public String descripitifElement() {
		return "CasePorte";
	}

}
