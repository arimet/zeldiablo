package modele;
/**
 * Classe Aventurier qui definit les monstres de notre jeux.
 */

public class Monstre extends Ennemi {
	/**
	 * Attributs
	 */
	private int intelligence;

	/**
	 * Constructeur de Monstre
	 * @param x
	 * @param y
	 * @param a
	 */
	public Monstre(int x,int y, int a){
		super(x,y);
		this.intelligence=a;
		this.pDv=5;
	}


	/**
	 * Methode qui retourne le descriptif de Monstre
	 */
	public String descripitifElement(){
		return "Monstre";
	}

	/**
	 * Methode retournant l intelligence du Monstre
	 */
	public int getIntelligence(){
		return this.intelligence;
	}
}
