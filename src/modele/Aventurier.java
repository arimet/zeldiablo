package modele;

import java.util.ArrayList;

/**
 * Classe Aventurier qui definit le hero de notre jeux.
 * @author rimet1u
 *
 */
public class Aventurier extends Personnage {
	/**
	 * Attributs
	 */
	public static final int MAX_PV = 100;
	public static int NOMBRE_SALLES_PARCOURUES = 0;
	public ArrayList<ElementInventaire> listeInventaire;
	
	
	
	/**
	 * Constructeur creant un Aventurier
	 * @param x
	 * @param y
	 * @param pdv
	 * 
	 */
	public Aventurier(int x,int y){
		super(x,y);
		this.pDv=MAX_PV;
		this.listeInventaire=new ArrayList<ElementInventaire>();
	}

	
	
	/**
	 *Methode qui definit la salle du Personnage
	 */
	@Override
	public void setSalle(Salle salle_nouvelle) {
		super.setSalle(salle_nouvelle);

		NOMBRE_SALLES_PARCOURUES++;
	}
	
	
	/**
	 * Methode designant la classe Aventurier
	 * @return String
	 */
	public String descripitifElement() {
		return "Aventurier";
	}

	
	/**
	 * Methode ajoutant un element a l inventaire de l aventurier
	 * @param e
	 */
	public void ajoutElemInventaire(ElementInventaire e){
		this.listeInventaire.add(e);
	}

	
	/**
	 * Methode ajoutant de la vie a l aventurier
	 * @param a
	 */
	public void ajouterVie(int a){
		if (this.pDv+a<MAX_PV){
			this.pDv+=a;
		}else{  
			this.pDv=MAX_PV;
		}
	}
}