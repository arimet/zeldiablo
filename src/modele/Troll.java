package modele;
/**
 * Classe definissant Troll herite de Ennemi

 */
public class Troll extends Ennemi{
	/**
	 * Constructeur de troll
	 * @param x
	 * @param y
	 */
	public Troll(int x,int y){
		super(x,y);
		this.pDv=5;
	}
	
	public String descripitifElement(){
		return "Troll";
	}
	
//Ce qui suit empeche le troll de bouger dans tout les cas
	public void allerDroite(){
		this.seTranslater(0, 0);
	}
	

	public void allerGauche(){
		this.seTranslater(0, 0);
	}
	

	public void allerHaut(){
		this.seTranslater(0,0);
	}
	

	public void allerBas(){
		this.seTranslater(0,0);
	}

	@Override
	public int getIntelligence() {
		return 0;
	}

}
