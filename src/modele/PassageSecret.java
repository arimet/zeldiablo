package modele;
/**
 * Classe definissant PassageSecret qui possede l inteface ElementSalle
 */
public class PassageSecret implements ElementSalle {
	/**
	 * Attributs
	 */
	String affichage = "Mur";
	String etat;
	int liaison;
	
	
	/**
	 * Constructeur de passageSecret
	 * @param liaison
	 */
	public PassageSecret(int liaison){
		this.liaison = liaison;
		this.etat = "ferme";
	}
	
	
	/**
	 * Change les attributs affichage et etat
	 */
	public void ouverture(){
		affichage = "PassageSecret";
		etat = "ouvert";
	}
	
	
	/**
	 * Change les attributs affichage et etat
	 */
	public void fermeture(){
		affichage = "Mur";
		etat = "ferme";
	}
	
	
	/**
	 * Retourne la liaison du passageSecret
	 * @return
	 */
	public int getLiaison(){
		return liaison;
	}
	
	/**
	 * Retourne l etat du passage secret
	 * @return
	 */
	public String getEtat(){
		return etat;
	}

	@Override
	public String descripitifElement() {
		return affichage;
	}
}
