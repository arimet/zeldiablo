package modele;
/**
 * Classe definisant CasePiege qui possede l'interface ElementSalle
 * @author Anthony
 *
 */
public class CasePiege implements ElementSalle {
	/**
	 * Attribut
	 */
	private boolean active=false;
	
	
	/**
	 * Methode retournant le descriptif de la classe
	 */
	public String descripitifElement(){
		String s = "CasePiege " + this.active;
		return s;
	}
	
	/**
	 * Methode qui met active a true
	 */
	public void declenchement(){
		this.active=true;
	}
	
	/**
	 * Methode qui retourne le boolean active
	 * @return
	 */
	public boolean estDeclancher(){
		return this.active;
	}
}